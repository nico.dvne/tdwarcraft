﻿using System;
using TypePeon;

namespace TypePeasant
{
    public class Peasant
    {
        private string Race;
        private string Faction;
        private int HitPoint;
        private int Armor;

        public Peasant(string Race, string Faction , int HitPoint, int Armor)
        {
            this.Race = Race;
            this.Faction = Faction;
            this.HitPoint = HitPoint;
            this.Armor = Armor;
        }
    
         public string getRace()
        {
            return this.Race;
        }
        public string getFaction()
        {
            return this.Faction;
        }
        public int getHitPoint()
        {
            return this.HitPoint;
        }
        public int getArmor()
        {
            return this.Armor;
        }
        public string grunt()
        {
            return "Grouaaah";
        }
        public string sayHello()
        {           
            return string.Format("Hey I'm an {0} in the {1} faction,I've {2} of HitPoint and {3} of armor", this.Race, this.Faction, this.HitPoint, this.Armor);
        }
        public string talk()
        {
            return "Coucou, je suis un paysan et je n'aime pas me battre";
        }
        public void setRace(string race)
        {
           this.Race = race;
        }
        public void setFaction(string faction)
        {
            this.Faction=faction;
        }
        public void setHitPoint(int HitPoint)
        {
            this.HitPoint=HitPoint;
        }
        public void setArmor(int armor)
        {
            this.Armor=armor;
        }
        public string talkToPeasant(Peasant peasant)
        {
            return String.Format("Bonjour monsieur le {0},je suis Alex le {1}", peasant.getRace(), this.getRace());
        }
        public string talkToPeon(Peon peon)
        {
            return String.Format("Bonjour mon ennemi le {0}, je viens en paix,je suis un {1} de la faction {2}", peon.getRace(), this.getRace(), this.getFaction());
        }
    }
}