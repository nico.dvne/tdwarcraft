﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



using TypePeasant;
using TypePeon;

namespace Warcraft_Console
{
    

    public class Program
    {
        public static void Affichage_regle()
        {
            Console.WriteLine("Veuillez faire le choix : ");
            Console.WriteLine("a :Affichage des Infos du Peon");
            Console.WriteLine("b: Affichage des TalkTo du Peon");
            Console.WriteLine("c : Affichage des Talk, Grunt, sayHello du Peon");
            Console.WriteLine("");
            Console.WriteLine("d: Affichage des Infos du Peasant");
            Console.WriteLine("e : Affichage des TalkTo du Peasant");
            Console.WriteLine("f: Affichage des Talk, Grunt, SayHello du Peasant");
        }
        public static void Main(string[] args)
        {
            string choix;
            Peon Nicolas = new Peon("Humain", "Cycliste", 300, 10);
            Peon Joseph = new Peon("Lion", "Animal", 40, 10);
            Peasant Test = new Peasant("Elfe", "Footballeur", 40, 10);
            Affichage_regle();
            Console.WriteLine("Saisir votre choix : ");
            choix = Console.ReadLine();

            switch(choix)
            {
                case "a":
                    Console.WriteLine(Nicolas.getRace());
                    Console.WriteLine(Nicolas.getFaction());
                    Console.WriteLine(Nicolas.getHitPoint());
                    Console.WriteLine(Nicolas.getArmor());
                    break;
                case "b":                    
                    Console.WriteLine(Nicolas.TalkToPeasant(Test));
                    Console.WriteLine(Nicolas.TalkToPeon(Joseph));
                    break;
                case "c":
                    Console.WriteLine(Nicolas.talk());
                    Console.WriteLine(Nicolas.grunt());
                    Console.WriteLine(Nicolas.sayHello());
                    break;
                case "d":
                    Console.WriteLine(Test.getRace());
                    Console.WriteLine(Test.getFaction());
                    Console.WriteLine(Test.getHitPoint());
                    Console.WriteLine(Test.getArmor());
                    break;
                case "e":
                    Console.WriteLine(Test.talkToPeasant(Test));
                    Console.WriteLine(Test.talkToPeon(Joseph));
                    break;
                case "f":
                    Console.WriteLine(Test.talk());
                    Console.WriteLine(Test.grunt());
                    Console.WriteLine(Test.sayHello());
                    break;
            }
            Console.ReadKey();


        }
    }
}
