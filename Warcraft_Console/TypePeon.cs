﻿using System;
using TypePeasant;

namespace TypePeon
{
    public class Peon
    {
        private string race;
        private string faction;
        private int HitPoint;
        private int Armor;

        public Peon(string Race,string Faction, int HitPoint, int Armor)
        {
            this.race = Race;
            this.faction=Faction;
            this.HitPoint = HitPoint;
            this.Armor = Armor;
        }
        public string getRace()
        {
            return this.race;
        }
        public string getFaction()
        {
            return this.faction;
        }
        public int getHitPoint()
        {
            return this.HitPoint;
        }
        public int getArmor()
        {
            return this.Armor;
        }
        public string grunt()
        {
            return "grrr";
        }
        public string talk()
        {         
           return string.Format("Bonjour,je suis un {0}", this.race);
        }
        public string sayHello()
        {
            return String.Format("Hello, je suis un {0}", this.race);
        }
        public void SetRace(string race)
        {
            this.race = race;
        }
        public void setFaction(string faction)
        {
            this.faction = faction;
        }
        public void setHitPoint(int HitPoint)
        {
            this.HitPoint = HitPoint;
        }
        public void setArmor(int armor)
        {
            this.Armor = armor;
        }
        public string TalkToPeon(Peon Jean)
        {
            return String.Format("Bonjour mon ami le {0}, je suis Alex le {1}", Jean.getRace(), this.getRace());
        }
        public string TalkToPeasant(Peasant Jean)
        {
            return String.Format("Bonjour mon ennemi le {0},je viens en paix, je suis un {1} et la faction {2}", Jean.getRace(), this.getRace(), this.getFaction());
        }
    }
}