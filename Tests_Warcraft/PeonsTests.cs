﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TypePeon;
using TypePeasant;

namespace Tests_Warcraft
{
    [TestClass]
    public class PeonsTests
    {
        [TestMethod]
        public void TestPeon()
        {
            Peon Jean = new Peon("Orc", "Horde", 30, 0);
            Assert.AreEqual("Orc", Jean.getRace());
            Assert.AreEqual("Horde", Jean.getFaction());
            Assert.AreEqual(30, Jean.getHitPoint());
            Assert.AreEqual(0, Jean.getArmor());
        }
        [TestMethod]
        public void TestPeongetRace()
        {
            Peon Jean = new Peon("Orc", "Horde", 30, 0);
            Assert.AreEqual("Orc", Jean.getRace());
        }
        [TestMethod]
        public void TestPeongetFaction()
        {
            Peon Jean = new Peon("Orc", "Horde", 30, 0);
            Assert.AreEqual("Horde", Jean.getFaction());
        }
        [TestMethod]
        public void TestPeongetHitPoint()
        {
            Peon Jean = new Peon("Orc", "Horde", 30, 0);
            Assert.AreEqual(30, Jean.getHitPoint());
        }
        [TestMethod]
        public void TestPeongetArmor()
        {
            Peon Jean = new Peon("Orc", "Horde", 30, 0);
            Assert.AreEqual(0, Jean.getArmor());
        }
        [TestMethod]
        public void TestPeonGrunt()
        {
            Peon Jean = new Peon("Orc", "Horde", 30, 0);
            Assert.AreEqual("grrr", Jean.grunt());
        }
        [TestMethod]
        public void TestPeonTalk()
        {
            Peon Jean = new Peon("Orc", "Horde", 30, 0);
            Assert.AreEqual("Bonjour,je suis un Orc", Jean.talk());
        }
        [TestMethod]
        public void Test_Peon_SetRace()
        {
            Peon Jean = new Peon("Orc", "Horde", 30, 0);
            Jean.SetRace("Homme");
            Assert.AreEqual("Homme", Jean.getRace());
        }
        [TestMethod]
        public void TestPeasanSetFaction()
        {
            Peon Alex = new Peon("Human", "Alliance", 30, 0);
            Alex.setFaction("Rebelle");
            Assert.AreEqual("Rebelle", Alex.getFaction());
        }
        [TestMethod]
        public void TestPeasanSetHitPoint()
        {
            Peon Alex = new Peon("Human", "Alliance", 30, 0);
            Alex.setHitPoint(45);
            Assert.AreEqual(45, Alex.getHitPoint());
        }
        [TestMethod]
        public void TestPeonSetArmor()
        {
            Peon Alex = new Peon("Human", "Alliance", 30, 0);
            Alex.setArmor(200);
            Assert.AreEqual(200, Alex.getArmor());
        }
        [TestMethod]
        public void TestPeonTalkToPeon()
        {
            Peon Alex = new Peon("Human", "Alliance", 30, 0);
            Peon Jean = new Peon("Elfe", "Allicance", 50, 20);
            Assert.AreEqual("Bonjour mon ami le Elfe, je suis Alex le Human", Alex.TalkToPeon(Jean));
        }
        [TestMethod]
        public void TestPeonTalkToPeasant()
        {
            Peon Alex = new Peon("Human", "Alliance", 30, 0);
            Peasant Romain = new Peasant("Animal", "Dark", 500, 10);
            Assert.AreEqual("Bonjour mon ennemi le Animal,je viens en paix, je suis un Human et la faction Alliance",Alex.TalkToPeasant(Romain));
        }
        [TestMethod]
        public void TestPeonSayHello()
        {
            Peon Alex=new Peon("Human", "Alliance", 30, 0);
            Assert.AreEqual("Hello, je suis un Human", Alex.sayHello());
        }
    }

}
