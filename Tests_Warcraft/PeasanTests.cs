﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TypePeasant;
using TypePeon;

namespace Tests_Warcraft
{
    [TestClass]
    public class PeasanTests
    {
        [TestMethod]
        public void TestPeasantConstructor()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Assert.AreEqual("Human", Alex.getRace());
            Assert.AreEqual("Alliance", Alex.getFaction());
            Assert.AreEqual(30, Alex.getHitPoint());
            Assert.AreEqual(0, Alex.getArmor());
        }
        [TestMethod]
        public void TestPeasantGetRace()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Assert.AreEqual("Human", Alex.getRace());
        }
        [TestMethod]
        public void TestPeasantGetFaction()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Assert.AreEqual("Alliance", Alex.getFaction());
        }
        [TestMethod]
        public void TestPeasantGetHitPoint()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Assert.AreEqual(30, Alex.getHitPoint());
        }
        [TestMethod]
        public void TestPeasantGetArmor()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Assert.AreEqual(0, Alex.getArmor());
        }      
        
            [TestMethod]
        public void TestPeasantGrunt()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Assert.AreEqual("Grouaaah", Alex.grunt());
        }
        [TestMethod]
        public void TestPeasantsayHello()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Assert.AreEqual("Hey I'm an Human in the Alliance faction,I've 30 of HitPoint and 0 of armor", Alex.sayHello());
        }
        [TestMethod]
        public void TestPeasantTalk()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Assert.AreEqual("Coucou, je suis un paysan et je n'aime pas me battre", Alex.talk());
        }
        [TestMethod]
        public void TestPeasanSetRace()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Alex.setRace("Metamorph");
            Assert.AreEqual("Metamorph", Alex.getRace());
        }
        [TestMethod]
        public void TestPeasanSetFaction()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Alex.setFaction("Rebelle");
            Assert.AreEqual("Rebelle", Alex.getFaction());
        }
        [TestMethod]
        public void TestPeasanSetHitPoint()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Alex.setHitPoint(45);
            Assert.AreEqual(45, Alex.getHitPoint());
        }
        [TestMethod]
        public void TestPeasanSetArmor()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Alex.setArmor(200);
            Assert.AreEqual(200, Alex.getArmor());
        }
        [TestMethod]
        public void TestPeasantTalkToPeasant()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 0);
            Peasant Louis = new Peasant("Elfe", "Alliance", 55, 20);
            Assert.AreEqual("Bonjour monsieur le Elfe,je suis Alex le Human", Alex.talkToPeasant(Louis));
        }
        [TestMethod]
        public void TestPeasantTalkToPeon()
        {
            Peasant Alex = new Peasant("Human", "Alliance", 30, 40);
            Peon John = new Peon("Orc", "Dark", 50, 40);
            Assert.AreEqual("Bonjour mon ennemi le Orc, je viens en paix,je suis un Human de la faction Alliance", Alex.talkToPeon(John));
        }
    }
   }

